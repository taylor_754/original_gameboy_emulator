class Gameboy {
	constructor (data)
	{
		this.cart = new Cartridge(data);
		this.memory = new Memory(this.cart);
		this.cpu = new Cpu(this.memory);
	}

	start ()
	{
		this.cpu.run();
	}
}