class Cpu {
	constructor (mem) {
		this.a = 0x00;
		this.b = 0x00;
		this.c = 0x13;
		this.d = 0x00;
		this.e = 0xD8;
		this.e = 0x00;
		this.f = 0xB0;
		this.h = 0x10;
		this.l = 0x4D;
		this.flags = {zero:false, subtract:false, half_carry:false, carry:false};
		this.pc = 0x0100;
		this.sp = 0xFFFE;
		this.opcode = 0x00;
		this.memory = mem;

		//states
		this.stopped = false;
		this.halted = false;
	}

	run () {
		//main logic loop here
		for (;;)//so it doesn't check a condition on each iteration
		{
			setTimeout (function () {
				if (!this.stopped && !this.halted)
				{
					this.opcode = this.memory.readMemory (this.pc);
					this.pc = (this.pc +1 ) & 0xFFFF;
				}
				else {
					this.opcode = 0x00;//do nothig until STOP command is lifted by joypad
				}

				//giant switch statement
				switch (opcode)
				{
					case 0x00:
						//NOP
					break;


					case 0x01:
						//LD BC, $aabb
						this.c = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						this.b = this.memory.readMemory (this.pc);
					break;


					case 0x02:
						//LD (BC),A
						//initialize a number with the higher value, bit shift to the left, and add the lower half
						this.memory.writeMemory(this.bc, this.a);
					break;


					case 0x03:
						//INC BC
						this.bc = (this.bc + 1) & 0xFFFF;
					break;


					case 0x04:
						//INC B
						if (halfCarry (this.b, 1)){this.flags.half_carry = true;}
						this.flags.zero = false;
						this.flags.subtract = false;
						this.b = (this.b  + 1) & 0xFF;//AND'ing with 0xFF (all ones) will cause it to wrap to 0
					break;


					case 0x05:
						//DEC B
						this.b = ADD(this.b, -1);
					break;


					case 0x06:
						//LD B, $xx
						this.b = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x07:
						//RLCA - bit shift A to the left, bit 7 is now carry bit
						this.flags.carry = (this.a > 0x7F);//means bit 7 is 1
						this.a = ((this.a << 1) & 0xFF) | (this.a >> 7);//ignore bits now to the left of bit 7
						this.flags.zero = (this.a == 0)? true : false;
						this.flags.half_carry = false;
						this.flags.subtract = false;
					break;


					case 0x08:
						//LD ($aabb), SP
						//this.memory.writeMemory((this.b << 8 | this.c), this.a);
						var addr = ( (this.memory.readMemory (this.pc + 1) * 0xFFFF) << 8) + (this.memory.readMemory(this.pc));//the address to write to
						this.memory.writeMemory (addr, this.sp & 0xFF);
						this.memory.writeMemory ((addr + 1) & 0xFFFF, this.sp >>> 8);
						this.pc = (this.pc + 2) & 0xFFFF;
					break;


					case 0x09:
						//ADD HL,BC
						var working = ADD(this.l, this.c);
						this.l = working;
						if (this.flags.carry)
						{
							working = working | 0x100
						}
						working += ADD(this.h, this.b);
						this.h = working >> 8;
					break;


					case 0x0A:
						//LD A,(BC)
						this.a = this.memory.readMemory ( this.bc );
					break;


					case 0x0B:
						//DEC BC
						this.bc = (this.bc - 1) & 0xFFFF;
					break;

					case 0x0C:
						//INC C
						this.c = ADD(this.c, 1);
					break;


					case 0x0D:
						//DEC C
						this.c = ADD(this.c, -1);
					break;


					case 0x0E:
						//LD C,$xx
						this.c = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x0F:
						//RRCA
						this.flags.carry = this.a & 0x01;
						this.a = this.a >> 1;
						this.flags.zero = (this.a == 0) ? true : false;
						this.flags.half_carry = false;
						this.falgs.subtract = false;
					break;


					case 0x10:
						//STOP
						this.stopped = true;
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x11:
						//LD DE,$aabb
						this.e = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						this.d = this.memory.readMemory (this.pc);
					break;


					case 0x12:
						//LD (DE), A
						this.memory.writeMemory(this.de, this.a);
					break;


					case 0x13:
						//INC DE
						this.de = (this.de + 1) & 0xFFFF;
					break;


					case 0x14:
						//INC D
						this.d = ADD(this.d, 1);
					break;


					case 0x15:
						//DEC D
						this.d = ADD(this.d, -1);
					break;


					case 0x16:
						//LD D,$xx
						this.d = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x17:
						//RLA
						var carry = (this.flags.carry)?1:0;
						this.flags.carry = (this.a > 0x7F);
						this.a = ((this.a << 1) | carry) & 0xFF;
						this.flags.half_carry = false;
						this.flags.subtract = false;
						this.flags.zero = (this.a == 0)? true : false;
					break;

					case 0x18:
						//JR $xx
						this.pc = (this.pc + this.memory.readMemory(this.pc)) & 0xFFFF;
					break;


					case 0x19:
						//ADD HL,DE
						var working = ADD(this.l, this.e);
						this.l = working;
						if (this.flags.carry)
						{
							working = working | 0x100
						}
						working += ADD(this.h, this.d);
						this.h = working >> 8;
					break;


					case 0x1A:
						//LD A,(DE)
						this.a = this.memory.readMemory (this.de);
					break;


					case 0x1B:
						//DEC DE
						this.de = (this.de - 1) & 0xFFFF;
					break;


					case 0x1C:
						//INC E
						this.e = ADD(this.e, 1);
					break;


					case 0x1D:
						//DEC E
						this.e = ADD(this.e, -1);
					break;


					case 0x1E:
						//LD E,$xx
						this.e = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x1F:
						//RRA
						var carry = (this.flags.carry)?1:0;
						this.a = ((this.a >> 1) | (carry << 7));
						this.flags.carry = ((this.a & 1) == 1)? 1 : 0;
						this.flags.half_carry = false;
						this.flags.subtract = false;
						this.flags.zero = (this.a == 0)? true : false;
					break;


					case 0x20:
						//JR NZ,$xx
						var val = this.memory.readMemory(this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						if (!this.flags.zero)
						{
							this.pc = (this.pc + val) & 0xFFFF;
						}
					break;


					case 0x21:
						//LD HL,$aabb
						this.l = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						this.h = this.memory.readMemory (this.pc);
					break;


					case 0x22:
						//LD (HLI), A
						//translates to 
						//LD (HL), A
						//INC HL
						var addr = (this.h << 8) | this.l;
						this.memory.writeMemory (addr, this.a);
						this.hl = (this.hl + 1) & 0xFFFF;
					break;


					case 0x23:
						//INC HL
						this.hl = (this.hl + 1) & 0xFFFF;
					break;


					case 0x24:
						//INC H
						this.h = ADD(this.h, 1);
					break;


					case 0x25:
						//DEC H
						this.h = ADD(this.h, -1);
					break;


					case 0x26:
						//LD H,$xx
						this.h = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x27:
						//DAA
						//convert value in register A to BCD (Binary-Coded Decimal)
						//not 100% on this code - taken from https://forums.nesdev.com/viewtopic.php?t=15944
						if (!this.flags.subtract) //addition occurred
						{
							if (this.flags.carry || this.a > 0x99) { this.a += 0x60; this.flags.carry = true; }
							if (this.flags.half_carry || (this.a & 0x0F) > 0x09) { this.a += 0x06;}
						}
						else//subtraction occurred
						{
							if (this.flags.carry) { a -= 0x60; }
							if (this.flags.half_carry ) { this.a -= 0x06;}
						}
						this.flags.zero = (this.a == 0)? true: false;
						this.flags.half_carry = false;
					break;


					case 0x28:
						//JR Z,$xx
						var val = this.memory.readMemory(this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						if (this.flags.zero)
						{
							this.pc = (this.pc + val) & 0xFFFF;
						}
					break;


					case 0x29:
						//ADD HL,HL
						var working = ADD(this.l, this.l);
						this.l = working;
						if (this.flags.carry)
						{
							working = working | 0x100
						}
						working += ADD(this.h, this.h);
						this.h = working >> 8;
					break;


					case 0x2A:
						//LD A,(HLI)
						// LD A,(HL) - INC HL
						this.a = this.memory.readMemory (((this.h << 8) | this.l));
						this.hl = ( this.hl + 1 ) & 0xFFFF;
					break;


					case 0x2B:
						//DEC HL
						this.hl = ( this.hl - 1 ) & 0xFFFF;
					break;


					case 0x2C:
						//INC L
						this.l = ADD(this.l, 1);
					break;


					case 0x2D:
						//DEC L
						this.l = ADD(this.l, -1);
					break;


					case 0x2E:
						//LD L,$xx
						this.l = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x2F:
						//CPL
						//COMPLEMEMT A (inverse all bits)
						this.a = ~this.a & 0xFF;
						this.flags.subtract = true;
						this.flags.half_carry = true;
					break;





					case 0x30:
						//JR NC,$xx
						var val = this.memory.readMemory(this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						if (!this.flags.carry)
						{
							this.pc = (this.pc + val) & 0xFFFF;
						}
					break;


					case 0x31:
						//LD SP,$aabb
						var s = this.memory.readMemory (this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						var.p = this.memory.readMemory (this.pc);
						this.sp = (s << 8) | p;
					break;


					case 0x32:
						//LD (HLD), A
						//translates to
						//LD (HL), A
						//DEC HL
						this.memory.writeMemory (this.hl, this.a);
						this.hl = ( this.hl-1 ) & 0xFFFF;
					break;


					case 0x33;
						//INC SP
						this.sp = (this.sp + 1) & 0xFFFF;
					break;


					case 0x34:
						//INC (HL)
						this.memory.writeMemory (this.hl, this.memory.readMemory( (this.hl) + 1) & 0xFFFF );
					break;


					case 0x35:
						//DEC (HL)
						this.memory.writeMemory (this.hl, this.memory.readMemory( (this.hl) - 1) & 0xFFFF );
					break;


					case 0x36:
						//LD (HL),$xx
						this.memory.writeMemory (this.hl, this.memory.readMemory(this.pc));
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x37:
						//SCF - Set Carry Flag
						//zero flag not affected
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = true;
					break;


					case 0x38:
						//JR C,$xx
						var val = this.memory.readMemory(this.pc);
						this.pc = (this.pc + 1) & 0xFFFF;
						if (this.flags.carry)
						{
							this.pc = (this.pc + val) & 0xFFFF;
						}
					break;


					case 0x39:
						//ADD HL,SP
						var working = ADD(this.l, (this.sp & 0xFF));
						this.l = working;
						if (this.flags.carry)
						{
							working = working | 0x100
						}
						working += ADD(this.h, this.sp >> 8);
						this.h = working >> 8;
					break;


					case 0x3A:
						//LD A,(HLD)
						//LD A, (HL)
						//DEC HL
						this.a = this.memory.readMemory (this.hl);
						this.hl = (this.hl + 1) & 0xFFFF
					break;


					case 0x3B:
						//DEC SP
						this.sp = (this.sp - 1) & 0xFFFF;
					break;


					case 0x3C:
						//INC A
						this.a = ADD(this.a, 1);
					break;


					case 0x3D:
						//DEC A
						this.a = ADD(this.a, -1);
					break;


					case 0x3E:
						//LD A,$xx
						this.a = this.memory.readMemory (this.memory.readMemory (this.pc));
						this.pc = (this.pc + 1) & 0xFFFF;
					break;


					case 0x3F:
						//CCF - Complement carry flag
						this.flags.carry = !this.flags.carry;
						this.flags.subtract = false;
						this.flags.half_carry = false;
					break;


					case 0x40:
						//LD B,B
						//this.b = this.b; getter and setters will not like this
					break;


					case 0x41:
						//LD B,C
						this.b = this.c;
					break;


					case 0x42:
						//LD B,D
						this.b = this.d;
					break;


					case 0x43:
						//LD B,E
						this.b = this.e;
					break;


					case 0x44:
						//LD B,H
						this.b = this.h;
					break;


					case 0x45:
						//LD B,L
						this.b = this.l;
					break;


					case 0x46:
						//LD B,(HL)
						this.b = this.memory.readMemory (this.hl);
					break;


					case 0x47:
						//LD B,A
						this.b = this.a;
					break;


					case 0x48:
						//LD C,B
						this.c = this.b;
					break;


					case 0x49:
						//LD C,C
						//this.c = this.c;
					break;


					case 0x4A:
						//LD C,D
						this.c = this.d;
					break;


					case 0x4B:
						//LD C,E
						this.c = this.e;
					break;


					case 0x4C:
						//LD C,H
						this.c = this.h;
					break;


					case 0x4D:
						//LD C,L
						this.c = this.l;
					break;


					case 0x4E:
						//LD C,(HL)
						this.c = this.memory.readMemory (this.hl);
					break;


					case 0x4F:
						//LD C,A
						this.c = this.a;
					break;




					case 0x50:
						//LD D,B
						this.d = this.b;
					break;


					case 0x51:
						//LD D,C
						this.d = this.c;
					break;


					case 0x52:
						//LD D,D
						//this.d = this.d;
					break;


					case 0x53:
						//LD D,E
						this.d = this.e;
					break;


					case 0x54:
						//LD D,H
						this.d = this.h;
					break;


					case 0x55:
						//LD D,L
						this.d = this.l;
					break;


					case 0x56:
						//LD D,(HL)
						this.d = this.memory.readMemory (this.hl);
					break;


					case 0x57:
						//LD D,A
						this.d = this.a;
					break;


					case 0x58:
						//LD E,B
						this.e = this.b;
					break;


					case 0x59:
						//LD E,C
						this.e = this.c;
					break;


					case 0x5A:
						//LD E,D
						this.e = this.d;
					break;


					case 0x5B:
						//LD E,E
						//this.e = this.e;
					break;


					case 0x5C:
						//LD E,H
						this.e = this.h;
					break;


					case 0x5D:
						//LD E,L
						this.e = this.l;
					break;


					case 0x5E:
						//LD E,(HL)
						this.e = this.memory.readMemory (this.hl);
					break;


					case 0x5F:
						//LD E,A
						this.e = this.a;
					break;




					case 0x60:
						//LD H,B
						this.h = this.b;
					break;


					case 0x61:
						//LD H,C
						this.h = this.c;
					break;


					case 0x62:
						//LD H,D
						this.h = this.d;
					break;


					case 0x63:
						//LD H,E
						this.h = this.e;
					break;


					case 0x64:
						//LD H,H
						//this.h = this.h;
					break;


					case 0x65:
						//LD H,L
						this.h = this.l;
					break;


					case 0x66:
						//LD H,(HL)
						this.h = this.memory.readMemory (this.hl);
					break;


					case 0x67:
						//LD H,A
						this.h = this.a;
					break;


					case 0x68:
						//LD L,B
						this.l = this.b;
					break;


					case 0x69:
						//LD L,C
						this.l = this.c;
					break;


					case 0x6A:
						//LD L,D
						this.l = this.d;
					break;


					case 0x6B:
						//LD L,E
						this.l = this.e;
					break;


					case 0x6C:
						//LD L,H
						this.l = this.h;
					break;


					case 0x6D:
						//LD L,L
						//this.l = this.l;
					break;


					case 0x6E:
						//LD L,(HL)
						this.l = this.memory.readMemory (this.hl);
					break;


					case 0x6F:
						//LD L,A
						this.l = this.a;
					break;




					case 0x70:
						//LD (HL),B
						this.memory.writeMemory (this.hl, this.b);
					break;


					case 0x71:
						//LD (HL),C
						this.memory.writeMemory (this.hl, this.c);
					break;


					case 0x72:
						//LD (HL),D
						this.memory.writeMemory (this.hl, this.d);
					break;


					case 0x73:
						//LD (HL),E
						this.memory.writeMemory (this.hl, this.e);
					break;


					case 0x74:
						//LD (HL),H
						this.memory.writeMemory (this.hl, this.h);
					break;


					case 0x75:
						//LD (HL),L
						this.memory.writeMemory (this.hl, this.l);
					break;


					case 0x76:
						//HALT - halt until interrupt occurs
						//if DI (only on original GB), this will not halt but will skip the next instruction
						//if EI then wait until the next interrupt
						this.halted = true;
					break;


					case 0x77:
						//LD (HL),A
						this.memory.writeMemory (this.hl, this.l);
					break;


					case 0x78:
					//LD A,B
					this.a = this.b;
					break;


					case 0x79:
						//LD A,C
						this.a = this.c;
					break;


					case 0x7A:
						//LD A,D
						this.a = this.d;
					break;


					case 0x7B:
						//LD A,E
						this.a = this.d;
					break;


					case 0x7C:
						//LD A,H
						this.a = this.h;
					break;


					case 0x7D:
						//LD A,L
						this.a = this.l;
					break;


					case 0x7E:
						//LD A,(HL)
						this.a = this.memory.readMemory (this.hl);
					break;


					case 0x7F:
						//LD A,A
						//this.a = this.a;
					break;




					case 0x80:
						//ADD A,B
						this.a = ADD(this.a, this.b);
					break;


					case 0x81:
						//ADD A,C
						this.a = ADD(this.a, this.c);
					break;


					case 0x82:
						//ADD A,D
						this.a = ADD(this.a, this.d);
					break;


					case 0x83:
						//ADD A,E
						this.a = ADD(this.a, this.e);
					break;


					case 0x84:
						//ADD A,H
						this.a = ADD(this.a, this.h);
					break;


					case 0x85:
						//ADD A,L
						this.a = ADD(this.a, this.l);
					break;


					case 0x86:
						//ADD A,(HL)
						this.a = ADD(this.a, this.memory.readMemory(this.hl));
					break;


					case 0x87:
						//ADD A,A
						this.a = ADD(this.a, this.a);
					break;


					case 0x88:
						//ADC A,B
						this.a = ADC(this.a, this.b);
					break;


					case 0x89:
						//ADC A,C
						this.a = ADC(this.a, this.c);
					break;


					case 0x8A:
						//ADC A,D
						this.a = ADC(this.a, this.d);
					break;


					case 0x8B:
						//ADC A,E
						this.a = ADC(this.a, this.e);
					break;


					case 0x8C:
						//ADC A,H
						this.a = ADC(this.a, this.h);
					break;


					case 0x8D:
						//ADC A,L
						this.a = ADC(this.a, this.l);
					break;


					case 0x8E:
						//ADC A,(HL)
						this.a = ADC(this.a, this.memory.readMemory(this.hl));
					break;


					case 0x8F:
						//ADC A,A
						//this.a = ADC(this.a, this.a);
					break;




					case 0x90:
						//SUB B
						this.a = ADD(this.a, -this.b);
					break;


					case 0x91:
						//SUB C
						this.a = ADD(this.a, -this.c);
					break;


					case 0x92:
						//SUB D
						this.a = ADD(this.a, -this.d);
					break;


					case 0x93:
						//SUB E
						this.a = ADD(this.a, -this.c);
					break;


					case 0x94:
						//SUB H
						this.a = ADD(this.a, -this.h);
					break;


					case 0x95:
						//SUB L
						this.a = ADD(this.a, -this.l);
					break;


					case 0x96:
						//SUB (HL)
						this.a = ADD(this.a, -this.memory.readMemory(this.hl));
					break;


					case 0x97:
						//SUB A
						this.a = ADD(this.a, -this.a);
					break;


					case 0x98:
						//SBC B
						this.a = SBC(this.b);
					break;


					case 0x99:
						//SBC C
						this.a = SBC(this.c);
					break;


					case 0x9A:
						//SBC D
						this.a = SBC(this.d);
					break;


					case 0x9B:
						//SBC E
						this.a = SBC(this.e);
					break;


					case 0x9C:
						//SBC H
						this.a = SBC(this.h);
					break;


					case 0x9D:
						//SBC L
						this.a = SBC(this.l);
					break;


					case 0x9E:
						//SBC (HL)
						this.a = SBC(this.memory.readMemory(this.hl));
					break;


					case 0x9F:
						//SBC A
						this.a = SBC(this.a);
					break;




					case 0xA0:
						//AND B
						this.a = this.a & this.b;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA1:
						//AND C
						this.a = this.a & this.c;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA2:
						//AND D
						this.a = this.a & this.d;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA3:
						//AND E
						this.a = this.a & this.e;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA4:
						//AND H
						this.a = this.a & this.h;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA5:
						//AND L
						this.a = this.a & this.l;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA6:
						//AND (HL)
						this.a = this.a & this.memory.readMemory(this.hl);
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA7:
						//AND A
						this.a = this.a & this.a;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = this.a & 0x10 == 0x10;
						this.flags.carry = false;
					break;


					case 0xA8:
						//XOR B
						this.a = this.a ^ this.b;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;


					case 0xA9:
						//XOR C
						this.a = this.a ^ this.c;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;
					case 0xAA:
						//XOR D
						this.a = this.a ^ this.d;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xAB:
						//XOR E
						this.a = this.a ^ this.e;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xAC:
						//XOR H
						this.a = this.a ^ this.h;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xAD:
						//XOR L
						this.a = this.a ^ this.l;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xAE:
						//XOR (HL)
						this.a = this.a ^ this.memory.readMemory(this.hl);
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xAF:
						//XOR A
						this.a = this.a ^ this.a;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					


					case 0xB0:
						//OR B
						this.a = this.a | this.b;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB1:
						//OR C
						this.a = this.a | this.c;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB2:
						//OR D
						this.a = this.a | this.d;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB3:
						//OR E
						this.a = this.a | this.e;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB4:
						//OR H
						this.a = this.a | this.h;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB5:
						//OR L
						this.a = this.a | this.l;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB6:
						//OR (HL)
						this.a = this.a | this.memory.readMemory(this.hl);
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB7:
						//OR A
						this.a = this.a | this.a;
						this.flags.zero = (this.a == 0)? true:false;
						this.flags.subtract = false;
						this.flags.half_carry = false;
						this.flags.carry = false;
					break;

					
					case 0xB8:
						//CP B
						CP (this.b);
					break;

					
					case 0xB9:
						//CP C
						CP (this.c);
					break;

					
					case 0xBA:
						//CP D
						CP (this.d);
					break;

					
					case 0xBB:
						//CP E
						CP (this.e);
					break;

					
					case 0xBC:
						//CP H
						CP (this.h);
					break;

					
					case 0xBD:
						//CP L
						CP (this.l);
					break;

					
					case 0xBE:
						//CP (HL)
						CP (this.memory.readMemory(this.hl));
					break;

					
					case 0xBF:
						//CP A
						CP (this.a);
					break;

					


					case 0xC0:
						//RET NZ
						if (!this.flags.zero)
						{
							this.pc = (this.memory.readMemory (this.sp + 1) & 0xFFFF) << 8 | (this.memory.readMemory(this.sp)) & 0xFFFF;
							this.sp = (this.sp + 2) & 0xFFFF;
						}
					break;

					
					case 0xC1:
						//POP BC
						this.bc = pop();
					break;


					case 0xC2:
						//JP NZ,$aabb
						if (!this.flags.zero)
						{	
							var aabb = this.memory.readMemory( this.pc ) << 8 | this.memory.readMemory( (this.pc+1) && 0xFFFF );
							this.pc = aabb;
						}
					break;

					case 0xC3:
						//JP $aabb
						var aabb = this.memory.readMemory( this.pc ) << 8 | this.memory.readMemory( (this.pc+1) && 0xFFFF );
						this.pc = aabb;
					break;

					case 0xC4:
						//CALL NZ,$aabb
						if (!this.flags.zero)
						{
							var temp_pc = this.memory.readMemory( (this.pc+1) && 0xFFFF ) << 8 | this.memory.readMemory( (this.pc) && 0xFFFF );
							this.pc = (this.pc + 2) & 0xFFFF;
							this.sp = (this.sp - 1) & 0xFFFF;
							this.memory.writeMemory (this.sp, this.pc >> 8);
							this.sp = (this.sp - 1) & 0xFFFF;
							this.memory.writeMemory (this.sp, this.pc & 0xFF);
							this.pc = temp_pc;
						}
					break;
					
					case 0xC5:
					//PUSH BC
					break;
					case 0xC6:
					//ADD A,$xx
					break;
					case 0xC7:
					//RST $00
					break;
					case 0xC8:
					//RET Z
					break;
					case 0xC9:
					//RET
					break;
					case 0xCA:
					//JP Z,$aabb
					break;
					case 0xCB:
					//extended instruction set - separate switch here
					//no interrupt should throw here
						this.pc++;
						var subcode = this.memory.readMemory (this.pc);
						switch(subcode)
						{

						}
					break;
					case 0xCC:
					//CALL Z,$aabb
					break;
					case 0xCD:
					//CALL $aabb
					break;
					case 0xCE:
					//ADC A,$xx
					break;
					case 0xCF:
					//RST $08
					break;


					case 0xD0:
					//RET NC
					break;
					case 0xD1:
						//POP DE
						this.de = pop ();
					break;

					case 0xD2:
					//JP NC,$aabb
					break;
					case 0xD3:
					//NO OPCODE
					break;
					case 0xD4:
					break;
					case 0xD5:
					//PUSH DE
					break;
					case 0xD6:
					//SUB $xx
					break;
					case 0xD7:
					//RST $10
					break;
					case 0xD8:
					//RET C
					break;
					case 0xD9:
					//RETI
					break;
					case 0xDA:
					//JP C,$aabb
					break;
					case 0xDB:
					//NO OPCODE
					break;
					case 0xDC:
					//CALL C,$aabb
					break;
					case 0xDD:
					//NO OPCODE
					break;
					case 0xDE:
					//SBC A,$xx
					break;
					case 0xDF:
					//RST $18
					break;


					case 0xE0:
					//LD ($xx),A
					break;
					case 0xE1:
						//POP HL
						this.hl = pop();
					break;

					case 0xE2:
					//LD (C),A
					break;
					case 0xE3:
					//NO OPCODE
					break;
					case 0xE4:
					//NO OPCODE
					break;
					case 0xE5:
					//PUSH HL
					break;
					case 0xE6:
					//AND $xx
					break;
					case 0xE7:
					//RST $20
					break;
					case 0xE8:
					//ADD SP,xx
					break;
					case 0xE9:
					//JP (HL)
					break;
					case 0xEA:
					//LD ($aabb),A
					break;
					case 0xEB:
					//NO OPCODE
					break;
					case 0xEC:
					//NO OPCODE
					break;
					case 0xED:
					//NO OPCODE
					break;
					case 0xEE:
					//XOR $xx
					break;
					case 0xEF:
					//RST $28
					break;


					case 0xF0:
					//LD A,($xx)
					break;
					case 0xF1:
						//POP AF
						this.af = pop();
					break;

					case 0xF2:
					//LD A,(C)
					break;
					case 0xF3:
					//DI
					break;
					case 0xF4:
					//NO OPCODE
					break;
					case 0xF5:
					//PUSH AF
					break;
					case 0xF6:
					//OR $xx
					break;
					case 0xF7:
					//RST $30
					break;
					case 0xF8:
					//LD HL,SP
					break;
					case 0xF9:
					//LD SP,HL
					break;
					case 0xFA:
					//LD A,($aabb)
					break;
					case 0xFB:
					//EI
					break;
					case 0xFC:
					//NO OPCODE
					break;
					case 0xFD:
					//NO OPCODE
					break;
					case 0xFE:
					//CP $xx
					break;
					case 0xFF:
					//RST $38
					break;

				}


				//if (counter <= 0)
			}, 0);
			//alert (this.opcode);
		}
	}

	ADD (a, b) //8-bit addition - use twice for 16 bit addition
	{
		var sum = (a + b);
		this.flags.carry = (sum > 0xFF) ? true : false;
		this.flags.half_carry = (halfCarry(a,b)) ? true : false;
		this.flags.zero = (sum == 0) ? true : false;
		this.flags.subtract = (a < 0 || b < 0) ? true : false;
		return sum & 0xFF;
	}

	ADC (a, b)
	{
		var sum = (a + b) + ((this.flags.carry)1:0);
		this.flags.carry = (sum > 0xFF) ? true : false;
		this.flags.half_carry = (halfCarry(a,b)) ? true : false;
		this.flags.zero = (sum == 0) ? true : false;
		this.flags.subtract = (a < 0 || b < 0) ? true : false;
		return sum & 0xFF;
	}

	SBC (n)
	{
		var val = this.a - (n + ((this.flags.carry)1:0));
		this.flags.carry = (val > 0xFF) ? true : false;
		this.flags.half_carry = (halfCarry(a,b)) ? true : false;
		this.flags.zero = (val == 0) ? true : false;
		this.flags.subtract = true;
		return val & 0xFF;
	}

	CP (n)
	{
		var val = (this.a - n) & 0xFF;
		this.flags.zero = (val == 0) ? true : false;
		this.subtract = true;
		this.carry = (n > this.a)?true:false;
		this.half_carry = halfCarry (this.a, n);
	}

	halfCarry (a, b)
	{
		return (((a & 0xF) + (b & 0xF)) & 0x10) == 0x10;
	}

	//returns the popped value and moves the stack pointer
	pop ()
	{
		var a = this.memory.readMemory (this.sp);
		this.sp = (this.sp + 1) & 0xFFFF;
		var.b = this.memory.readMemory (this.sp);
		this.sp = (this.sp + 1) & 0xFFFF;
		return = (b << 8) | a;
	}

	interruptsEnabled ()
	{
		return;
	}


	get a ()
	{
		return this.a;
	}
	set a (val)
	{
		this._a = val;
	}

	get b ()
	{
		return this.b;
	}
	set b (val)
	{
		this._b = val;
	}

	get c ()
	{
		return this.c;
	}
	set c (val)
	{
		this._c = val;
	}

	get d ()
	{
		return this.d;
	}
	set d (val)
	{
		this._d = val;
	}

	get e ()
	{
		return this.e;
	}
	set e (val)
	{
		this._e = val;
	}

	get f ()
	{
		return this.f;
	}
	set f (val)
	{
		this._f = val;
	}

	get h ()
	{
		return this.h;
	}
	set h (val)
	{
		this._h = val;
	}

	get l ()
	{
		return this.l;
	}
	set l (val)
	{
		this._l = val;
	}

	/*get pc ()
	{
		return this.pc;
	}
	set pc (val)
	{
		this._pc = val;
	}*/

	get sp ()
	{
		return this.sp;
	}
	set sp (val)
	{
		this._sp = val;
	}
	//////// TODO : Update this to use bit operators to concat numbers since string oprations are slow
	get af ()
	{
		return ( (this.a << 8) | this.f);
	}
	set af (val)
	{
		this.a = (val >>> 8) & 0xFF;
		this.f = val - (this.a << 8) & 0xFF;	
	}

	get bc ()
	{
		return ( (this.b << 8) | this.c);
	}
	set bc (val)
	{
		this.b = (val >>> 8) & 0xFF;
		this.c = val - (this.b << 8) & 0xFF;
	}

	get de ()
	{
		return ( (this.d << 8) | this.e);
	}
	set de (val)
	{
		this.d = (val >>> 8) & 0xFF;
		this.e = val - (this.d << 8) & 0xFF;
	}

	get hl ()
	{
		return ( (this.h << 8) | this.l);
	}
	set hl (val)
	{
		this.h = (val >>> 8) & 0xFF;
		this.l = val - (this.h << 8) & 0xFF;
	}

	get f ()
	{
		ret = 0x00;
		if (this.flags.zero) ret += 0x80;
		if (this.flags.subtract) ret += 0x40;
		if (this.flags.half_carry) ret += 0x20;
		if (this.flags.carry) ret += 0x10;

		return ret;
	}

	set f (val)
	{
		//make sure the right four bits are always 0
		this.f = val;
		this.f = (this.f >>> 4) << 4;
		this.flags.zero = (this.f & 0x80);
		this.flags.subtract = (this.f & 0x40);
		this.flags.half_carry = (this.f & 0x20);
		this.flags.carry = (this.f & 0x10);
	}

	 
}

/*hexStr='7F';
num = parseInt(hexStr,16);   // num now holds 127)*/