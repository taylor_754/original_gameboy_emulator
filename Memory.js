class Memory {
	constructor (cart) {
		this.memory = new Int8Array(0xFFFF);
		this.cartridge = cart;
	}

	readMemory (addr)
	{
		if (addr <= 0xFFFF)
		{
			if ( (addr >= 0x0000 && addr <= 0x7FFF) || (addr >= 0xA000 && addr <= 0xBFFF))
			{
				return (this.cartridge.readData(addr));
			}
			else 
			{
				return this.memory [addr];
			}
		}
		else 
		{
			return -1;
		}
	}

	writeMemory (addr, val)
	{
		if (addr <= 0xFFFF)
		{
			this.memory[addr] = val;
		}
		if (addr >= 0xC000 && addr <= 0xDFFF)
		{
			this.memory[addr+0x2000] = val;
		}
		if (addr >= 0x0000 && addr <=0x7FFF)
		{
			//get the cartidge to handle this, the MBC chip will be changing modes
		}
	}
}